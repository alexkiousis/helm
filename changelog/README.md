changelog
=========
A Helm chart to deploy Changelog

Current chart version is `0.1.0`



## Chart Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.bitnami.com/bitnami | postgresql | 8.9.6 |

## Chart Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| fullnameOverride | string | `""` |  |
| image.pullPolicy | string | `"IfNotPresent"` |  |
| image.registry | string | `"registry.gitlab.com"` |  |
| image.repository | string | `"alexkiousis/changelog"` |  |
| image.tag | string | `"c9ec9b9b"` |  |
| ingress.annotations | object | `{}` |  |
| ingress.enabled | bool | `true` |  |
| ingress.hosts[0].host | string | `"prod.changelog.alexk.town"` |  |
| ingress.hosts[0].paths[0] | string | `"/"` |  |
| ingress.tls | list | `[]` |  |
| nameOverride | string | `""` |  |
| postgresql.enabled | bool | `true` |  |
| postgresql.postgresqlDatabase | string | `"changelog"` |  |
| postgresql.postgresqlPassword | string | `"changelog"` |  |
| postgresql.postgresqlUsername | string | `"changelog"` |  |
| replicaCount | int | `1` |  |
| service.port | int | `8080` |  |
| service.type | string | `"LoadBalancer"` |  |
| settings.CHANGELOG_DATABASE_URL | string | `"postgres://changelog:changelog@changelog-postgresql:5432/changelog"` |  |
| settings.CHANGELOG_DEBUG | string | `"False"` |  |
| settings.CHANGELOG_HOSTS | string | `"*"` |  |
| settings.CHANGELOG_SECRET_KEY | string | `"jammdl1y6#yeaaw@6=**jl2o)jcddp$aro^n_-8zw#!9&^u@_c"` |  |
